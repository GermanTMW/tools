#!/bin/bash

if [ ! "$1" ]; then
	TEMP_NPC_ADD="./"
else
	if [ "$1" == "help" ] || [ $1 == "?" ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
		echo "Usage: $0 -d 'Path' (absolute or relative to $0)"
		exit -1
	elif [ "$1" == "-d" ] && [ $2 != "" ]; then
		TEMP_NPC_ADD="$2"
		if [ ! -d $TEMP_NPC_ADD ]; then
			echo "Path doesn't exist, trying to make it!"
			mkdir $TEMP_NPC_ADD > /dev/null 2>&1
			if [ ! -d $TEMP_NPC_ADD ]; then
				echo "could not create Path... no rights?"
				exit -1
			fi
		fi
	fi	
fi

noinput(){
	echo "there where no input at: $1"
	exit -1
}

echo -n "NPC Name: "
read npcName
	if [ "$npcName" == "" ]; then
		noinput "NPC name"
	fi
	if [ -f "$TEMP_NPC_ADD$npcName.txt" ]; then
		echo "File exists, don't overwrite anything with script!"
		exit -1
	fi

echo -n "MapName: "
read mapName
	if [ "$mapName" == "" ]; then		
		noinput "MapName"
	fi

echo -n "Coordinate X: "
read cordX
	if [ "$cordX" == "" ]; then		
		noinput "coordinate X"
	fi

echo -n "Coordinate Y: "
read cordY
	if [ "$cordY" == "" ]; then
		noinput "coordinate Y"
	fi

echo -n "Sprite Name/Index: "
read npcSprite
	if [ "$npcSprite" == "" ]; then
		noinput "npcSprite"
	fi

touch "$TEMP_NPC_ADD$npcName.txt"
echo "//" >> $TEMP_NPC_ADD$npcName.txt
echo "// - $mapName $cordX $cordX $npcName $npcSprite" >> $TEMP_NPC_ADD$npcName.txt
echo "//" >> $TEMP_NPC_ADD$npcName.txt
echo "$mapName,$cordX,$cordY,0	script	$npcName	$npcSprite,{" >> $TEMP_NPC_ADD$npcName.txt
echo "" >> $TEMP_NPC_ADD$npcName.txt
echo "//generated code" >> $TEMP_NPC_ADD$npcName.txt
echo "" >> $TEMP_NPC_ADD$npcName.txt
echo "} //EOF" >> $TEMP_NPC_ADD$npcName.txt
echo -e "\e[32m NPC: $npcName was created in $TEMP_NPC_ADD \e[0m"
exit 0

